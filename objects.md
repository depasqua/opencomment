# OpenComment API Specification

# Objects

## User

A user is a secondary object that has usernames, an e-mail address, experience points*, and personalization attributes such as an avatar picture.

Users comment open source code and rate comments made by other users.

### Example User object

```js
{
    "id": "1", 						// ID is a String
    "username": "kevincoughlin",
    "name": "Kevin Coughlin",
    "email": "coughli6@tcnj.edu",
    "joined": "2012-09-05",
    "experience": 725 				// Experience is an Integer

       "activity": {
       		"count": 2,
           	"opencomments": [{
               "id": "3",
               "code_id": "apache123",
               "body": "This method prints 'Hello World' to the command line interface upon execution",
               "created": "2012-09-05",
               "last_modified": "2012-09-05 16:42:12",
               "last_modified_by": "kevincoughlin",
               "likes": 15,
               "dislikes": 2
           	}],
           "achievements": [{
               "name": "First Comment",
               "id": "achievement_01",
               "experience": 25,
               "image_url": "http://placekitten.com.s3.amazonaws.com/homepage-samples/200/138.jpg",
               "description": "Awarded after your first comment in OpenComment",
               "body": "Congratulations! You submitted your first comment!",
           }],
        }
    },
}
```

### User fields
    "id": "1", 						// ID is a String
    "username": "kevincoughlin",
    "name": "Kevin Coughlin",
    "email": "coughli6@tcnj.edu",
    "joined": "2012-09-05",
    "experience": 725 				// Experience is an Integer
