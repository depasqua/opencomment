# Load the rails application
require File.expand_path('../application', __FILE__)

# Ajax Star-rating Gem
# https://github.com/beghbali/ajaxful-rating
gem "ajaxful_rating"

# Initialize the rails application
Comtor::Application.initialize!
