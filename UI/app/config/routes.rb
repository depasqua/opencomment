require 'api_constraints' # File for API Scope Configuration

Comtor::Application.routes.draw do

  get "pages/home"
  get "pages/find"
  get "pages/leaderboard"
  get "pages/welcome"
  
  match "home" => "pages#home"
  match "find" => "pages#find"
  match "leaderboard" => "pages#leaderboard"

  devise_for :users do get '/users/sign_out' => 'devise/sessions#destroy' end
    
  # Set root path
  root :to => "pages#welcome"
  

  resources :opencomments
  resources :evaluations
  
  resources :chunks do
    # Post Method for AJAX Ratings
    member do
      post 'rate'
    end
    resources :opencomments, :evaluations
  end

  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
      resources :opencomments, :chunks
    end
  end

  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'
end
