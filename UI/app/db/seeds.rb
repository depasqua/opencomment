# 
# Seed File to Populate OpenComment App's Database
# Used for testing purposes
# Last modified: 10/1/12
# 
Chunk.create(code: '
	public class Fibonacci {
	    public static long fib(int n) {
	        if (n <= 1) return n;
	        else return fib(n-1) + fib(n-2);
	    }

	    public static void main(String[] args) {
	        int N = Integer.parseInt(args[0]);
	        for (int i = 1; i <= N; i++)
	            System.out.println(i + ": " + fib(i));
	    }
	}',
	comment: '
	/*************************************************************************
	 *  Compilation:  javac Fibonacci.java
	 *  Execution:    java Fibonacci N
	 *
	 *  Computes and prints the first N Fibonacci numbers.
	 *
	 *  WARNING:  this program is spectacularly inefficient and is meant
	 *            to illustrate a performance bug, e.g., set N = 45.
	 *
	 *
	 *   % java Fibonacci 7
	 *   1: 1
	 *   2: 1
	 *   3: 2
	 *   4: 3
	 *   5: 5
	 *   6: 8
	 *   7: 13
	 *
	 *   Remarks
	 *   -------
	 *    - The 93rd Fibonacci number would overflow a long, but this
	 *      will take so long to compute with this function that we
	 *      dont bother to check for overflow.
	 *
	 *************************************************************************/'
)

Chunk.create(code: '
	public class UseArgument {

	    public static void main(String[] args) {
	        System.out.print("Hi, ");
	        System.out.print(args[0]);
	        System.out.println(". How are you?");
	    }

	}',
	comment: '
	/*************************************************************************
	 *  Compilation:  javac UseArgument.java
	 *  Execution:    java UseArgument yourname
	 *
	 *  Prints "Hi, Bob. How are you?" where "Bob" is replaced by the
	 *  command-line argument.
	 *
	 *  % java UseArgument Bob
	 *  Hi, Bob. How are you?
	 *
	 *  % java UseArgument Alice
	 *  Hi, Alice. How are you?
	 *
	 *************************************************************************/'
)

Chunk.create(code: '
	public class HelloWorld {

	    public static void main(String[] args) {
	        System.out.println("Hello, World");
	    }

	}',
	comment: '
	/*************************************************************************
	 *  Compilation:  javac HelloWorld.java
	 *  Execution:    java HelloWorld
	 *
	 *  Prints "Hello, World". By tradition, this is everyones first program.
	 *
	 *  % java HelloWorld
	 *  Hello, World
	 *
	 *  These 17 lines of text are comments. They are not part of the program;
	 *  they serve to remind us about its properties. The first two lines tell
	 *  us what to type to compile and test the program. The next line describes
	 *  the purpose of the program. The next few lines give a sample execution
	 *  of the program and the resulting output. We will always include such 
	 *  lines in our programs and encourage you to do the same.
	 *
	 *************************************************************************/'
)

Chunk.create(code: '
	public class Ruler { 
	    public static void main(String[] args) { 
	        String ruler1 = " 1 ";
	        String ruler2 = ruler1 + "2" + ruler1;
	        String ruler3 = ruler2 + "3" + ruler2;
	        String ruler4 = ruler3 + "4" + ruler3;
	        String ruler5 = ruler4 + "5" + ruler4;

	        System.out.println(ruler1);
	        System.out.println(ruler2);
	        System.out.println(ruler3);
	        System.out.println(ruler4);
	        System.out.println(ruler5);
	    }

	}',
	comment: '
	/*************************************************************************
	 *  Compilation:  javac Ruler.java
	 *  Execution:    java Ruler
	 *  
	 *  Prints the relative lengths of the subdivisions on a ruler.
	 * 
	 *  % java Ruler
	 *  1 
	 *  1 2 1 
	 *  1 2 1 3 1 2 1 
	 *  1 2 1 3 1 2 1 4 1 2 1 3 1 2 1 
	 *  1 2 1 3 1 2 1 4 1 2 1 3 1 2 1 5 1 2 1 3 1 2 1 4 1 2 1 3 1 2 1 
	 *
	 *************************************************************************/'
)