class CreateEvaluations < ActiveRecord::Migration
  def change
    create_table :evaluations do |t|
      t.integer :attribute_one
      t.integer :attribute_two
      t.integer :attribute_three
      t.integer :attribute_four
      t.integer :attribute_five
      t.integer :user_id
      t.integer :chunk_id

      t.timestamps
    end
  end
end
