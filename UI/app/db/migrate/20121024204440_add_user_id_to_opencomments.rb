class AddUserIdToOpencomments < ActiveRecord::Migration
  def change
    add_column :opencomments, :user_id, :integer
  end
end
