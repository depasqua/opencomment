class CreateOpencomments < ActiveRecord::Migration
  def change
    create_table :opencomments do |t|
      t.string :commenter
      t.text :body
      t.references :chunk

      t.timestamps
    end
    add_index :opencomments, :chunk_id
  end
end
