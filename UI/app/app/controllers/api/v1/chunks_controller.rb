module Api
  module V1
    class ChunksController < ApplicationController
      respond_to :json
      
      # List All Chunks
      def index
        respond_with Chunk.all
      end
      
      # Show Specific Chunk
      def show
        respond_with Chunk.find(params[:id])
      end
      
      # Create a new Chunk
      def create
        respond_with Chunk.create(params[:chunk])
      end
      
      # Update a Specific Chunk
      def update
        respond_with Chunk.update(params[:id], params[:chunks])
      end
      
      # Destroy a Specific Chunk
      def destroy
        respond_with Chunk.destroy(params[:id])
      end

    end
  end
end
