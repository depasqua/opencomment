module Api
  module V1
    class OpencommentsController < ApplicationController
      respond_to :json
      
      # List All Opencomments
      def index
        respond_with Opencomment.all
      end
      
      # Show Specific Opencomment
      def show
        respond_with Opencomment.find(params[:id])
      end
      
      # Create a new Opencomment
      def create
        respond_with Opencomment.create(params[:opencomment])
      end
      
      # Update a Specific Opencomment
      def update
        respond_with Opencomment.update(params[:id], params[:opencomments])
      end
      
      # Destroy a Specific Opencomment
      def destroy
        respond_with Opencomment.destroy(params[:id])
      end

    end
  end
end
