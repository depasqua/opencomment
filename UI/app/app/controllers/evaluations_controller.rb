class EvaluationsController < ApplicationController
  # Create new opencomment from chunk
  def create
    @chunk = Chunk.find(params[:chunk_id])
    @user = current_user
    @evaluation = @chunk.evaluations.create(params[:evaluation])
    @evaluation.user_id = current_user.id
    @evaluation.save!

    redirect_to chunk_path(@chunk)
  end
  
  # New opencomment from chunk
  def new
    @Chunk = Chunk.find(params[:chunk_id])
    @evaluation = Evaluation.new(:chunk => @Chunk)
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @Chunk }
    end
  end

  # Delete an evaluation
  def destroy
    @Evaluation = Evaluation.find(params[:id])
    @Evaluation.destroy

    respond_to do |format|
      format.html { redirect_to chunks_url }
      format.json { head :no_content }
    end
  end
end
