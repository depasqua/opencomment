class OpencommentsController < ApplicationController
    # Create new opencomment from chunk
    def create
      @chunk = Chunk.find(params[:chunk_id])
      @opencomment = @chunk.opencomments.create(params[:opencomment])
      @opencomment.user_id = current_user.id
      @opencomment.save!
      redirect_to chunk_path(@chunk)
    end
    
    # New opencomment from chunk
    def new
      @Chunk = Chunk.find(params[:chunk_id])
      @opencomment = Opencomment.new(:chunk => @Chunk)

      respond_to do |format|
        format.html # new.html.erb
        format.json { render :json => @Chunk }
      end
    end
end
