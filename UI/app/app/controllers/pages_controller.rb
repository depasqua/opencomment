class PagesController < ApplicationController
  # Presented to not logged in user as a call to action
  def welcome
  	if user_signed_in?
  		redirect_to :action => "home"
  	end
  end

  # Home page for signed in users
  def home
    @Opencomments = current_user.opencomments.all     # Get all of user's opencomments
    @Evaluations  = current_user.evaluations.all      # Get all of user's evaluations

    respond_to do |format|
    format.html # show.html.erb
      format.json { render :json => 
                    { 
                      :Opencomments   => @Opencomments,
                      :Evaluations    => @Evaluations,
                    }
                  }
    end
  end

  # Find friends of user
  def find
  end

  # Show leaderboard of users
  def leaderboard
  end
end
