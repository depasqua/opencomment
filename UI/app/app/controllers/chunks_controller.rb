class ChunksController < ApplicationController
  before_filter :authenticate_user!

  # Evaluate Code Chunk
  def eval
  end

  # Rate Code Chunk
  def rate
    @chunk = Chunk.find(params[:id])
    @chunk.rate(params[:stars], current_user, params[:dimension])
    render :update do |page|
      page.replace_html @chunk.wrapper_dom_id(params), ratings_for(@chunk, params.merge(:wrap => false))
      page.visual_effect :highlight, @chunk.wrapper_dom_id(params)
    end
  end
  
  # Comment Code Chunk
  def comment
  end

  # GET /chunks
  # GET /chunks.json
  # Show all Chunks
  def index
    @chunks = Chunk.all
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @chunks }
    end
  end

  # GET /chunks/1
  # GET /chunks/1.json
  # Show a Chunk
  def show
    @chunk = Chunk.find(params[:id])
    @Opencomments = Opencomment.all
    @Evaluations = Evaluation.all

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => { 
                                      :chunk          => @Chunk, 
                                      :opencomments   => @Opencomments,
                                      :evaluations    => @Evaluations,
                                    }}
    end
  end

  # GET /chunks/new
  # GET /chunks/new.json
  # Create a new chunk
  def new
    @Chunk = Chunk.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @Chunk }
    end
  end

  # GET /chunks/1/edit
  # Edit a chunk
  def edit
    @Chunk = Chunk.find(params[:id])
    @Opencomment = Opencomment.new( :chunk => @chunk )

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => { :chunk        => @Chunk, 
                                      :opencomment  => @Opencomment }}
    end
  end

  # POST /chunks
  # POST /chunks.json
  # Create a chunk
  def create
    @Chunk = Chunk.new(params[:Chunk])

    respond_to do |format|
      if @Chunk.save
        format.html { redirect_to @Chunk, :notice => 'Chunk was successfully created.' }
        format.json { render :json => @Chunk, :status => :created, :location => @Chunk }
      else
        format.html { render :action => "new" }
        format.json { render :json => @Chunk.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /chunks/1
  # PUT /chunks/1.json
  # Put a chunk
  def update
    @Chunk = Chunk.find(params[:id])

    respond_to do |format|
      if @Chunk.update_attributes(params[:Chunk])
        format.html { redirect_to @Chunk, :notice => 'Chunk was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @Chunk.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /chunks/1
  # DELETE /chunks/1.json
  # Delete a chunk
  def destroy
    @Chunk = Chunk.find(params[:id])
    @Chunk.destroy

    respond_to do |format|
      format.html { redirect_to chunks_url }
      format.json { head :no_content }
    end
  end
end
