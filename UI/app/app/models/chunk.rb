class Chunk < ActiveRecord::Base
  # Has many relationship with opencomments, will delete upon chunk destruction
  has_many :opencomments
  has_many :evaluations

  # Before destruction check that no opencomments are references so they are not lost
  before_destroy :ensure_not_referenced_by_any_opencomment

  # Accessible Model Attributes
  attr_accessible :code, :comment, :opencomment, :evaluation

  # Model Validation
  validates :code, :comment, presence: true

  # Ajaxful Rating
  ajaxful_rateable :dimensions => [:attr1, :attr2, :attr3, :attr4, :attr5]
  
  # Private Method Scope
  private
  # Reference checking method
  def ensure_not_referenced_by_any_opencomment
  	if opencomments.empty?
  		return true
  	else
  		errors.add(:base, 'Opencomments are present')
  		return false
  	end
  end
end
