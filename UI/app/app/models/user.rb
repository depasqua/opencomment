class User < ActiveRecord::Base
  # Has many relationship with opencomments, will delete upon chunk destruction
  has_many :opencomments
  has_many :evaluations

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :etc, :rememberable

  attr_accessible :email, :password, :first_name, :opencomment
end
