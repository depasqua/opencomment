class Evaluation < ActiveRecord::Base
  belongs_to :chunk
  belongs_to :user
  
  attr_accessible :attribute_one, :attribute_two, :attribute_three, :attribute_four, :attribute_five, :chunk, :user

  #validates_numericality_of :n, :only_integer => true, :message => "Only whole numbers allowed."
  #validates_inclusion_of :n, :in => 0..5, :message => "Must be between 0 and 5"
end
