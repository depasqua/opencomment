class Opencomment < ActiveRecord::Base
  belongs_to :chunk
  belongs_to :user
  
  attr_accessible :body, :commenter, :chunk, :user
end
