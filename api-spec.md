# OpenComment API Specification

## IMPORTANT!

This is a working document. Any api specification is free to change at any time.

## Description

The OpenComment project is a side project for [COMTOR](https://comtor.org), the Java source code analyzer.

## Table of Contents

* [Object Definitions](./objects.md)
* [Pipe Definitions]()
* [Authentication Guide]()

## Developers

* [Pete DePasquale](http://www.tcnj.edu/~depasqua/)
* [Kevin Coughlin](http://kevintcoughlin.com) ([github.com/KevinTCoughlin],(http://github.com/KevinTCoughlin), [@kevintcoughlin](http://twitter.com/kevintcoughlin), coughli6@tcnj.edu)
